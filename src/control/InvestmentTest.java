package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import model.BankAccount;
import view.InvestmentFrame;

public class InvestmentTest {
	private static final double INITIAL_BALANCE = 1000; /**��С�ȵ����Ẻattribute ��С�����ç�������set����������鹢ͧ�Թ�ѭ�� */
	ActionListener list; /**��û�С���ͺ�硵����յ���ê��� list ���ѧ�����˹��¤���������Ѻ�红����Ţͧ�ͺ�硵�*/
	InvestmentFrame frame ; /**��û�С���ͺ�硵����յ���ê��� frame ���ѧ�����˹��¤���������Ѻ�红����Ţͧ�ͺ�硵�*/
	BankAccount account = new BankAccount(INITIAL_BALANCE);/**��û�С��������ҧobject�ͧBankAccount �������control����ö��Ҷ֧����ʹ��е���õ�ҧ㹤���BankAccount 
	���set������balance=INITIAL_BALANCE*/
	
	class AddInterestListener implements ActionListener
    {
		
       public void actionPerformed(ActionEvent event)
       {
    	   double rate = Double.parseDouble(frame.rateField.getText()); /**��û�С�ȵ����Ẻlocal ����ժ������ rate �����纤���Ţ�ȹ��� */
    	   double interest = account.getBalance() * rate / 100 ; /**��û�С�ȵ����Ẻlocal ����ժ������ interest �����纤���Ţ�ȹ��� */
           account.deposit(interest) ; 
           frame.resultLabel.setText("balance: " + account.getBalance());
       }            
    }
	
	public static void main(String[] args) {
		new InvestmentTest() ;
	}
	
	public InvestmentTest() {
		frame = new InvestmentFrame() ; /**������ҧobject�ͧInvestment �������control����ö��Ҷ֧����ʹ��е���õ�ҧ㹤���Investment*/
		frame.pack() ;
		frame.setVisible(true) ;
		frame.setSize(frame.FRAME_WIDTH,frame.FRAME_HEIGHT) ;
		list = new AddInterestListener() ; /**������ҧobject�ͧAddInterestListener �������control����ö��Ҷ֧����ʹ��е���õ�ҧ㹤���AddInterestListener*/
		frame.setListener(list) ;
		frame.resultLabel.setText("balance: " + account.getBalance());

	}
}
