package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;

public class InvestmentFrame extends JFrame{
	
	public static final double DEFAULT_RATE = 5;
	public static final double INITIAL_BALANCE = 1000;
	
	public static final int FRAME_WIDTH = 450;
	public static final int FRAME_HEIGHT = 100;
	
	
	private JPanel panel ;
	private JLabel rateLabel ;
	public JLabel resultLabel;
	public JTextField rateField;
	private JButton button;

	public InvestmentFrame() {
		createFrame() ;
	}
	
	public void createFrame() {
	    //resultLabel = new JLabel("balance: " + account.getBalance());
		resultLabel = new JLabel("balance: ");
		createButton() ;
		createTextField() ;
		createPanel() ;
	}
	
	private void createTextField() {
		rateLabel = new JLabel("Interest Rate: ");
		
		final int FIELD_WIDTH = 10;
		rateField = new JTextField(FIELD_WIDTH);
		rateField.setText("" + DEFAULT_RATE);
	}
	
	public void createButton() {
		this.button = new JButton("Add Interest");
		//button.addActionListener(list);
	}
	
	public void createPanel() {
		panel = new JPanel();
		panel.add(rateLabel);
	    panel.add(rateField);
	    panel.add(this.button);
	    panel.add(resultLabel);      
	    add(panel);
	}
	
	public void setListener(ActionListener list) {
		button.addActionListener(list);
	}

}

