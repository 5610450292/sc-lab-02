package model;

public class BankAccount
{  
   private double balance;
   
   public BankAccount()
   {   
      balance = 1000;
   }
   
  public BankAccount(double initialBalance)
   {   
      this.balance = initialBalance;
   }
   
   public void deposit(double amount)
   {  
     this.balance+= amount ;
      
   }
  
   public void withdraw(double amount)
   {   
      this.balance -= amount ;
      
   }
   
   public double getBalance()
   {   
      return this.balance;
   }
}
